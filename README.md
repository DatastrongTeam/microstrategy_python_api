# MicroStrategy Python API

[![pypi](https://img.shields.io/pypi/v/mstr-python-api.svg)](https://pypi.org/project/config-wrangler/)
[![license](https://img.shields.io/github/license/arcann/mstr_python_api.svg)](https://github.com/arcann/mstr_python_api/blob/master/license.txt)


Python API library for interacting with MicroStrategy Intelligence Server and/or MicroStrategy Web Server.

Supported MicroStrategy sub-APIs

 - TaskProc API
 - COM API
 - REST API (Work in progress)

## Installation

Install using `pip install -U mstr-python-api` or `conda install mstr-python-api -c conda-forge`.

# Examples

See `examples` folder